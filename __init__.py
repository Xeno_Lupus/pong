from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.goose import Dict, Any
from st3m.input import InputState
from ctx import Context
import leds
import math
import random

log = logging.Log(__name__, level=logging.INFO)
log.info("start")

#positions/sizes
board_coord_left: int = -90
board_coord_right: int = 90
board_coord_top: int = -80
board_coord_bottom: int = 80
paddle_height: int = 30
paddle_width: int = 10
ball_height: int = 10
ball_width: int = ball_height
restart_note_top: int = -80

#petal ids
petal_restart: int = 0
petal_paddle_left_up: int = 8
petal_paddle_left_down: int = 6
petal_paddle_right_up: int = 2
petal_paddle_right_down: int = 4

#other settings
win_counter_increase_ms: int = 100

max_win_count = 10
brightness: int = 69 # 69 is default brightness
ball_speed: float = 0.1

#fix constants
MAX_LED_COUNT: int = 40
FIRST_LED_ID: int = 0
MIDDLE_LED_ID: int = int(MAX_LED_COUNT / 2)
PLAYER_LED_COUNT: int = int(MAX_LED_COUNT / 2 - 1)

class Color():
    def __init__(self, red: int, green: int, blue: int) -> None:
        self.red = red
        self.green = green
        self.blue = blue
    #enddef
#endclass

color_player_left = Color(255, 0, 0) #red
color_player_right = Color(0, 0, 255) #blue
color_background = Color(0, 0, 0) #black
color_playground_border = Color(255, 255, 255) #white
color_playground = Color(0, 0, 0) #black
color_ball = Color(0, 255, 0) #green
color_countdown = Color(255, 255, 255) #white
color_restart_note = Color(128, 128, 128) #gray
color_empty = Color(0, 0, 0) #black

class LED():
    def __init__(self) -> None:
        self._counter = 0
        self._timer = 0
    #enddef

    def drawGamePoints(self, ctx: Context, points_left: int, points_right: int) -> None:
        leds.set_rgb(FIRST_LED_ID, (color_player_right.red + color_player_left.red) / 2, (color_player_right.green + color_player_left.green) / 2, (color_player_right.blue + color_player_left.blue) / 2)
        for counter in range(PLAYER_LED_COUNT):
            if counter + 1 <= points_right * 2:
                color = color_player_right
            else:
                color = color_empty
            leds.set_rgb(counter + 1, color.red, color.green, color.blue)
            #endif
        #endfor
        leds.set_rgb(MIDDLE_LED_ID, (color_player_right.red + color_player_left.red) / 2, (color_player_right.green + color_player_left.green) / 2, (color_player_right.blue + color_player_left.blue) / 2)
        for counter in range(PLAYER_LED_COUNT):
            if counter + 1 <= points_left * 2:
                color = color_player_left
            else:
                color = color_empty
            leds.set_rgb(MAX_LED_COUNT - 1 - counter, color.red, color.green, color.blue)
            #endif
        #endfor
    #enddef

    def drawWin(self, ctx: Context, points_left: int, points_right: int) -> None:
        for counter in range(MAX_LED_COUNT):
            if points_left >= max_win_count:
                color = color_player_left
                index = MAX_LED_COUNT - 1 - counter #go counter clockwise
            else:
                color = color_player_right
                index = counter #go clockwise
            #endif
            if counter > self._counter:
                color = color_empty
            #endif
            leds.set_rgb(index, color.red, color.green, color.blue)
        #endfor
    #enddef

    def draw(self, ctx: Context, points_left: int, points_right: int) -> None:
        leds.set_brightness(brightness)
        if points_left >= max_win_count or points_right >= max_win_count:
            self.drawWin(ctx, points_left, points_right)
        else:
            self.drawGamePoints(ctx, points_left, points_right)
        #endif
        leds.update()
    #enddef

    def think(self, ins: InputState, delta_ms: int) -> None:
        self._timer = self._timer + delta_ms
        if self._timer > win_counter_increase_ms:
            self._timer = self._timer - win_counter_increase_ms

            self._counter = self._counter + 1
            if self._counter >= MAX_LED_COUNT:
                self._counter = -1
            #endif
        #endif
    #enddef
#endclass


class Paddle():
    def __init__(self, startX: int, petal_move_up: int, petal_move_down: int, color: Color) -> None:
        self._Y = -paddle_height / 2
        self._X = startX
        self._velocityY = ball_speed
        self._petal_move_up = petal_move_up
        self._petal_move_down = petal_move_down
        self._color = color
    #enddef

    def draw(self, ctx: Context):
        ctx.rgb(self._color.red, self._color.green, self._color.blue).rectangle(self._X, self._Y, paddle_width, paddle_height).fill()
    #enddef

    def think(self, ins: InputState, delta_ms: int) -> None:
        if ins.captouch.petals[self._petal_move_up].pressed:
            self._Y = self._Y - self._velocityY * delta_ms
        elif ins.captouch.petals[self._petal_move_down].pressed:
            self._Y = self._Y + self._velocityY * delta_ms
        
        if self.top() < board_coord_top:
            self._Y = board_coord_top - (self.top() - board_coord_top)
        elif self.bottom() > board_coord_bottom:
            self._Y = board_coord_bottom - (self.bottom() - board_coord_bottom) - paddle_height
        #endif
    #enddef

    def left(self) -> float:
        return self._X
    def right(self) -> float:
        return self._X + paddle_width
    def top(self) -> float:
        return self._Y
    def bottom(self) -> float:
        return self._Y + paddle_height
    def centerY(self) -> float:
        return self._Y + paddle_height / 2
#endclass


class Ball():
    def __init__(self) -> None:
        self._Y = -ball_height / 2
        self._X = -ball_width / 2
        self._velocityY = 0.0
        # go random towards one player
        self._velocityX = random.randint(0, 1)
        if self._velocityX == 0:
            self._velocityX = -1
        #endif
        self._velocityX = self._velocityX * ball_speed
    #enddef

    def draw(self, ctx: Context):
        ctx.rgb(color_ball.red, color_ball.green, color_ball.blue).rectangle(self._X, self._Y, ball_width, ball_height).fill()
    #enddef

    def think(self, ins: InputState, delta_ms: int, paddle_left: Paddle, paddle_right: Paddle) -> None:
        prevLeft = self.left()
        prevRight = self.right()
        # Movement X
        self._X = self._X + self._velocityX * delta_ms
        # Movement Y
        self._Y = self._Y + self._velocityY * delta_ms
        if self.bottom() >= board_coord_bottom:
            self.Y = board_coord_bottom - (self.bottom() - board_coord_bottom) - ball_height
            self._velocityY = -abs(self._velocityY)
        elif self.top() <= board_coord_top:
            self._Y = board_coord_top + (self.top() - board_coord_top)
            self._velocityY = abs(self._velocityY)
        #endif
        # Paddle Left
        if (self.left() <= paddle_left.right()
            and prevLeft >= paddle_left.right() #check only when we get behind the paddle this think
            and ((self.bottom() >= paddle_left.top() and self.bottom() <= paddle_left.bottom())
                or (self.top() >= paddle_left.top() and self.top() <= paddle_left.bottom()))):
            self._X = paddle_left.right() - (self.left() - paddle_left.right())
            
            pixel = self.centerY() - paddle_left.centerY()
            grad = pixel / (paddle_height / 2) * 45
            rad = grad * 2 * math.pi / 360 
            self._velocityX = math.cos(rad) * ball_speed
            self._velocityY = math.sin(rad) * ball_speed
        # Paddle Right
        elif (self.right() >= paddle_right.left()
            and prevRight <= paddle_right.left() #check only when we get behind the paddle this think
            and ((self.bottom() >= paddle_right.top() and self.bottom() <= paddle_right.bottom())
                or (self.top() >= paddle_right.top() and self.top() <= paddle_right.bottom()))):
            self._X = paddle_right.left() - (self.right() - paddle_right.left()) - ball_width
            
            pixel = self.centerY() - paddle_right.centerY()
            grad = pixel / (paddle_height / 2) * 45
            rad = grad * 2 * math.pi / 360 
            self._velocityX = -math.cos(rad) * ball_speed
            self._velocityY = math.sin(rad) * ball_speed
        #endif
    #enddef

    def left(self) -> float:
        return self._X
    def right(self) -> float:
        return self._X + ball_width
    def top(self) -> float:
        return self._Y
    def bottom(self) -> float:
        return self._Y + ball_height
    def centerY(self) -> float:
        return self._Y + ball_height / 2
#endclass


class CountDown():
    def __init__(self) -> None:
        self._countdown = 4000
    #enddef

    def drawCountDown(self, ctx: Context, rotate: float) -> None:
        ctx.save()
        ctx.rotate(rotate)
        ctx.rgb(color_countdown.red, color_countdown.green, color_countdown.blue)
        text = str(math.floor(self._countdown / 1000))
        scale = (self._countdown % 1000) / 1000 + 1
        ctx.scale(scale, scale)
        ctx.move_to(-ctx.text_width(text) / 2, ctx.font_size)
        ctx.text(text)
        ctx.restore()
    #enddef

    def draw(self, ctx: Context) -> None:
        if self.isFinished():
            return
        
        self.drawCountDown(ctx, 0)
        self.drawCountDown(ctx, math.pi)
    #enddef

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.isFinished():
            return
        
        self._countdown = self._countdown - delta_ms
    #enddef

    def isFinished(self) -> bool:
        return self._countdown <= 0
    #enddef
#endclass


class Playground():
    def drawPlayground(self, ctx: Context) -> None:
        ctx.rgb(color_background.red, color_background.green, color_background.blue).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(color_playground_border.red, color_playground_border.green, color_playground_border.blue).rectangle(board_coord_left - 1, board_coord_top - 1, board_coord_right - board_coord_left + 2, board_coord_bottom - board_coord_top + 2).fill()
        ctx.rgb(color_playground.red, color_playground.green, color_playground.blue).rectangle(board_coord_left, board_coord_top, board_coord_right - board_coord_left, board_coord_bottom - board_coord_top).fill()
    #enddef

    def drawPoints(self, ctx: Context, left_points: int, right_points: int) -> None:
        # points left
        ctx.save()
        ctx.rotate(math.pi / 2)
        ctx.rgb(color_player_left.red, color_player_left.green, color_player_left.blue)
        text = str(left_points)
        ctx.move_to( -ctx.text_width(text) / 2, board_coord_right / 2)
        ctx.text(text)
        ctx.restore()

        # points right
        ctx.save()
        ctx.rotate(math.pi / 2 * 3)
        ctx.rgb(color_player_right.red, color_player_right.green, color_player_right.blue)
        text = str(right_points)
        ctx.move_to( -ctx.text_width(text) / 2, board_coord_right / 2)
        ctx.text(text)
        ctx.restore()
    #enddef


    def draw(self, ctx: Context, left_points: int, right_points: int) -> None:
        self.drawPlayground(ctx)
        self.drawPoints(ctx, left_points, right_points)
    #enddef
#endclass


class Game():
    def __init__(self) -> None:
        self._playground = Playground()
        self._countdown = CountDown()
        self._led = LED()
        self._ball = Ball()
        self._paddle_left = Paddle(board_coord_left + 10, petal_paddle_left_up, petal_paddle_left_down, color_player_left)
        self._paddle_right = Paddle(board_coord_right - 10 - paddle_width, petal_paddle_right_up, petal_paddle_right_down, color_player_right)
        self._active = True
        self._left_points = 0
        self._right_points = 0
    #enddef

    def increasePointsLeft(self) -> None:
        self._left_points = self._left_points + 1
    #enddef
    def increasePointsRight(self) -> None:
        self._right_points = self._right_points + 1
    #enddef

    def drawRestartNote(self, ctx: Context) -> None:
        ctx.save()
        ctx.rgb(color_restart_note.red, color_restart_note.green, color_restart_note.blue)
        ctx.scale(0.5, 0.5)
        text = "Press top flower"
        ctx.move_to(-ctx.text_width(text) / 2, restart_note_top)
        ctx.text(text)
        text = "to restart"
        ctx.move_to(-ctx.text_width(text) / 2, restart_note_top + ctx.font_size + 2)
        ctx.text(text)
        ctx.restore()
    #enddef

    def draw(self, ctx: Context) -> None:
        self._led.draw(ctx, self._left_points, self._right_points)

        self._playground.draw(ctx, self._left_points, self._right_points)
        self._countdown.draw(ctx)
        self._paddle_left.draw(ctx)
        self._paddle_right.draw(ctx)
        self._ball.draw(ctx)

        if not self._active:
            self.drawRestartNote(ctx)
    #enddef

    def checkForPoints(self, ball: Ball) -> bool:
        if ball.right() >= board_coord_right:
            self.increasePointsLeft()
            self._active = False
        elif ball.left() <= board_coord_left:
            self.increasePointsRight()
            self._active = False
        #endif
    #enddef

    def checkPlayerWon(self) -> bool:
        return self._left_points >= max_win_count or self._right_points >= max_win_count
    #enddef

    def reset(self) -> None:
        self._ball = Ball()
        self._countdown = CountDown()
        self._active = True

        if self.checkPlayerWon():
            self._left_points = 0
            self._right_points = 0
        #endif
    #enddef

    def think(self, ins: InputState, delta_ms: int) -> None:
        if not self._active and ins.captouch.petals[petal_restart].pressed:
            self.reset()
        #endif

        self._countdown.think(ins, delta_ms)

        self._paddle_left.think(ins, delta_ms)
        self._paddle_right.think(ins, delta_ms)

        if self._active and self._countdown.isFinished():
            self._ball.think(ins, delta_ms, self._paddle_left, self._paddle_right)
            self.checkForPoints(self._ball)
        #endif

        self._led.think(ins, delta_ms)
    #enddef
#endclass

class App(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._game = Game()
    #enddef

    def draw(self, ctx: Context) -> None:
        self._game.draw(ctx)
    #enddef

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._game.think(ins, delta_ms)
    #enddef
#endclass
